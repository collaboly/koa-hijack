// Import
const send = require('koa-send');
const { toArray, isHtmlFile } = require('./utils');
const debug = require('debug')('koa-hijack:main');

/**
 * Main entry point for koa-hijack
 * @param {object} config options
 * @return {function} middleware
 * @api public
 */
module.exports = function(config) {
  const dirs = toArray(config.webroot);
  const opts = {
    defer: true,
    index: isHtmlFile(config.index) ? config.index : 'index.html'
  };
  // Export
  return async function(ctx, next) {
    
  };
};
