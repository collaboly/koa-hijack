# koa-hijack

This module allow you to do the same thing just like koa-serve (static serve) with some internal bug fixes. Plus allow you to do 1. Inject assets files (javascript, css) into your HTML document. 2. Virtually render files based on the request url.