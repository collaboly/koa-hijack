/**
 * Module dependencies.
 */

const debug = require('debug')('koa-send')
const resolvePath = require('resolve-path')
const createError = require('http-errors')
const assert = require('assert')
const fs = require('mz/fs')

const {
  normalize,
  basename,
  extname,
  resolve,
  parse,
  sep
} = require('path')
let lastErr;
let lastErrStatus;
/**
 * Expose `send()`.
 */

module.exports = send

/**
 * Send file at `path` with the
 * given `options` to the koa `ctx`.
 *
 * @param {Context} ctx
 * @param {String} path
 * @param {Object} [opts]
 * @return {Function}
 * @api public
 */
async function send (ctx, path, opts = {}) {
  assert(ctx, 'koa context required');
  assert(path, 'pathname required');
  // options
  debug('send "%s" %j', path, opts);
  // The root is now always an array
  const roots = Array.isArray(opts.root) ? opts.root : [opts.root];
  // const root = opts.root ? normalize(resolve(opts.root)) : ''
  const trailingSlash = path[path.length - 1] === '/'
  path = path.substr(parse(path).root.length)
  const index = opts.index
  const maxage = opts.maxage || opts.maxAge || 0
  const immutable = opts.immutable || false
  const hidden = opts.hidden || false
  const format = opts.format !== false
  const extensions = Array.isArray(opts.extensions) ? opts.extensions : false
  const brotli = opts.brotli !== false
  const gzip = opts.gzip !== false
  const setHeaders = opts.setHeaders
  if (setHeaders && typeof setHeaders !== 'function') {
    throw new TypeError('option setHeaders must be function')
  }
  // normalize path
  path = decode(path)
  if (path === -1) return ctx.throw(400, 'failed to decode')
  // index file support
  if (index && trailingSlash) path += index
  // From this point onward need to change to accept array of root(s)
  const result = getFileStats({
    roots,
    path,
    hidden,
    brotli,
    gzip,
    extensions,
    format,
    index
  });
  // that means the file is not found
  if (result.length) {
    // Success and found the file - serve it up
    const { 
      rootpath,
      stats,
      encodingExt 
    } = result[0];
    if (encodingExt!=='') {
      ctx.set('Content-Encoding', encodingExt==='gz' ? 'gzip' : encodingExt);
      ctx.res.removeHeader('Content-Length');
    }
    if (setHeaders) {
      setHeaders(ctx.res, rootpath, stats)
    }
    // stream
    ctx.set('Content-Length', stats.size)
    if (!ctx.response.get('Last-Modified')) {
      ctx.set('Last-Modified', stats.mtime.toUTCString())
    }
    if (!ctx.response.get('Cache-Control')) {
      const directives = ['max-age=' + (maxage / 1000 | 0)]
      if (immutable) {
        directives.push('immutable')
      }
      ctx.set('Cache-Control', directives.join(','))
    }
    if (!ctx.type) {
      ctx.type = type(rootpath, encodingExt)
    }
    ctx.body = fs.createReadStream(rootpath)
  } else {
    throw createError(lastErrStatus || 404, lastErr);
  }
  return rootpath;
}

/**
 * Break out the above loop method to filter out the unfound path
 * @param {object} params all the options 
 * @return {array} filtered array of the path and stats
 */
function getFileStats(params) {
  const {
    roots,
    path,
    hidden,
    brotli,
    gzip,
    extensions,
    format,
    index
  } = params;
  
  // loop start
  return roots.filter( root => {
    let encodingExt = '';
    path = resolvePath(root, path)
    // hidden file support, ignore
    if (!hidden && isHidden(root, path)) return
    // serve brotli file when possible otherwise gzipped file when possible
    if (ctx.acceptsEncodings('br', 'identity') === 'br' && brotli && (await fs.exists(path + '.br'))) {
      path = path + '.br'
      // ctx.set('Content-Encoding', 'br')
      // ctx.res.removeHeader('Content-Length')
      encodingExt = 'br'
    } else if (ctx.acceptsEncodings('gzip', 'identity') === 'gzip' && gzip && (await fs.exists(path + '.gz'))) {
      path = path + '.gz'
      // ctx.set('Content-Encoding', 'gzip')
      // ctx.res.removeHeader('Content-Length')
      encodingExt = 'gz'
    }
    if (extensions && !/\.[^/]*$/.exec(path)) {
      const list = [].concat(extensions)
      for (let i = 0; i < list.length; i++) {
        let ext = list[i]
        if (typeof ext !== 'string') {
          throw new TypeError('option extensions must be array of strings or false')
        }
        if (!/^\./.exec(ext)) ext = '.' + ext
        if (await fs.exists(path + ext)) {
          path = path + ext
          break
        }
      }
    }
    // stat
    let stats
    try {
      stats = await fs.stat(path)
      // Format the path to serve static file servers
      // and not require a trailing slash for directories,
      // so that you can do both `/directory` and `/directory/`
      if (stats.isDirectory()) {
        if (format && index) {
          path += '/' + index
          stats = await fs.stat(path)
        } else {
          return
        }
      }
      // return result
      return {
        rootpath: path,
        stats,
        encodingExt
      };
    } catch (err) {
      const notfound = ['ENOENT', 'ENAMETOOLONG', 'ENOTDIR']
      if (notfound.includes(err.code)) {
        lastErrStatus = 404;
        lastErr = err;
        // throw createError(404, err)
        return false;
      }
      debug('err.status 500', err);
      // err.status = 500
      // throw err
      lastErrStatus = 500;
      lastErr = err;
      return false;
    }
  });
};


/**
 * normalize resolve
 * @param {string} path directories
 * @return {string} resolved normalize path
 */
function normalizeResolve(path) {
  return normalize(resolve(path));
}

/**
 * Check if it's hidden.
 */

function isHidden (root, path) {
  path = path.substr(root.length).split(sep)
  for (let i = 0; i < path.length; i++) {
    if (path[i][0] === '.') return true
  }
  return false
}

/**
 * File type.
 */

function type (file, ext) {
  return ext !== '' ? extname(basename(file, ext)) : extname(file)
}

/**
 * Decode `path`.
 */

function decode (path) {
  try {
    return decodeURIComponent(path)
  } catch (err) {
    return -1
  }
}
