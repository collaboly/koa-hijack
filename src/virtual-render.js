/**
 * virtually render files based on url, also allow you to use lodash.template to render output
 * @param {object} config options
 * @return {function} middleware
 */
const debug = require('debug')('koa-hijack:virtual-render');

module.exports = function(config) {


  return async function(ctx, next) {
    await next();
    // Only catch certain methods
    if (ctx.method === 'HEAD' || ctx.method === 'GET') {
      const url = ctx.url;
    }
  }
}
