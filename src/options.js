/**
 * default options
 */
module.exports = {
    webroot: [],
    inject: {
        enable: false,
        source: [],
        body: [],
        head: []
    },
    render: {
        enable: false,
        files: {}
    }
};