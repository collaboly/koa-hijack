/**
 * File(s) injector for HTML document
 */
const debug = require('debug')('koa-hijack:file-inject');
const _ = require('lodash');
const { 
  getFilesToInject, 
  injectToHtml, 
  tagJs,
  searchIndexFile,
  isHtmlFile,
  headerParser,
  getDocLen,
  readDocument
} = require('./utils');
const {
 getFeatureScripts,
 renderScriptsMiddleware
} = require('./render-scripts-middleware');


/**
* @param {object} config the main config
* @return {function} middleware
* @api public
*/
module.exports = function(config) {
 let scripts = [];
 let features = {
   debugger: config.debugger.enable,
   reload: config.reload.enable,
   inject: config.inject.enable
 };
 const { socketIoJs, debuggerJs, stacktraceJsFile, reloadJs } = getFeatureScripts(
   config
 );
 if (features.debugger || features.reload) {
   scripts.push(socketIoJs);
 }
 if (features.debugger) {
   // @TODO if they change the debugger config
   // we might have to do additional checks here just in case
   scripts = scripts.concat([stacktraceJsFile, debuggerJs]);
 }
 if (features.reload) {
   // @2018-05-14 using our new reload method
   scripts.push(reloadJs);
 }
 const files = tagJs(scripts);
 // Next we add the fileInjector function here
 const { js, css } = getFilesToInject(config.inject);
 const contentType = 'text/html';
 // Export the middleware
 return async function(ctx, next) {
   if (ctx.method === 'HEAD' || ctx.method === 'GET') {
     if (headerParser(ctx.request, contentType)) {
       const p =
         ctx.path === '/'
           ? searchIndexFile(config)
           : isHtmlFile(ctx.path)
             ? ctx.path
             : false;
       if (p) {
         try {
           debug('use overwrite', ctx.url, ctx.path);
           const doc = await getHtmlDocument(p, _.compact([files, js]).join(''), css);
           ctx.status = 200;
           ctx.type = contentType + '; charset=utf8';
           ctx.length = getDocLen(doc);
           ctx.body = doc;
         } catch (err) {
           debug('get document error', err);
           ctx.throw(404, `Html file ${p} not found!`);
         }
         return;
       }
     }
   }
   await next();
 };
};

