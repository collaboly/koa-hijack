/**
 * Share methods
 */
const fs = require('fs');
const glob = require('glob');
const path = require('path');
const chalk = require('chalk');
const cheerio = require('cheerio');

/**
 * @param {array} files to wrap with tag
 * @param {string} ignorePath to strip out
 * @return {string} conccat them all
 */
const tagCss = (files, ignorePath) => {
  return files
    .map(file => {
      if (ignorePath) {
        file = file.replace(ignorePath, '');
      }
      return `<link rel="stylesheet" href="${file}" />`;
    })
    .join('\r\n');
};
/**
 * @param {array} files to wrap with tag
 * @param {string} ignorePath to strip out
 * @return {string} conccat them all
 */
const tagJs = (files, ignorePath) => {
  return files
    .map(file => {
      if (ignorePath) {
        file = file.replace(ignorePath, '');
      }
      return `<script type="text/javascript" src="${file}" defer></script>`;
    })
    .join('\r\n');
};

/**
 * @param {string} source to process
 * @return {array} result
 */
const processFiles = source => {
  let files = [];
  if (source.indexOf('*') > -1) {
    files = files.concat(glob.sync(source));
  } else {
    files = files.concat([source]);
  }
  return files;
};

/**
 * @param {string} name file
 * @return {boolean} true found css
 */
const isCss = name => name.toLowerCase().substr(-3) === 'css';

/**
 * @param {string} name file
 * @return {boolean} true found js
 */
const isJs = name => name.toLowerCase().substr(-2) === 'js';


/**
 * @param {mixed} source array or object
 * @return {object} js / css
 */
const getSource = source => {
  let head = [];
  let body = [];   
  if (_.isObject(source) && (source.head || source.body)) { // fixed array
    head = toArray(source.head);
    body = toArray(source.body);
    


  } else if (_.isString(source) || _.isArray(source)) {
    source = toArray(source);
    // Processing the object
    for (let i = 0, len = source.length; i < len; ++i) {
      let s = source[i];
      if (isCss(s)) {
        head = head.concat(processFiles(s));
      } else if (isJs(s)) {
        body = body.concat(processFiles(s));
      }
    }
  } 
  // output
  return {
    body: body.length ? body : false,
    head: head.length ? head : false
  };
};

/**
 * Prepare the css / js array to inject
 * @param {object} config the config.inject properties
 * @return {object} for use
 */
const getFilesToInject = function(config) {
  // @2018-05-07 disbale this check because we couldn't get the fileanme from the middleware
  // const target = getTarget(config.target);
  const { head, body } = getSource(config.source);
  // Const check = target && (js || css);
  if (!head || !body) {
    if (config.enable) {
      // Display an error inline here
      const msg = '[inject] Configuration is incorrect for inject to work!';
      debug('injector error', msg);
    }
    return { head: [], body: [] };
  }
  return {
    body: tagJs(js, config.ignorePath),
    head: tagCss(css, config.ignorePath)
  };
};

/**
 * @param {string} doc rendered html
 * @param {array} head of tag CSS
 * @param {array} body of tag Javascript
 * @return {string} overwritten HTML
 */
const injectToHtml = (doc, head, body) => {
  const html = typeof doc === 'string' ? doc : doc.toString('utf8');
  const $ = cheerio.load(html);
  $('body').append(js);
  $('head').append(css);
  return $.html();
};

/**
 * Breaking out the read function for the aynsc operation
 * @param {string} p path to file
 * @param {string} head tags
 * @param {string} body tags
 * @return {object} promise resolve string
 */
const getHtmlDocument = (p, head, body) => {
  return readDocument(p).then(data => {
    if (data) {
      return injectToHtml(data, head, body);
    }
  });
};

/**
 * The koa ctx object is not returning what it said on the documentation
 * So I need to write a custom parser to check the request content-type
 * @param {object} req the ctx.request
 * @param {string} type (optional) to check against
 * @return {mixed} Array or Boolean
 */
const headerParser = (req, type) => {
  try {
    const headers = req.headers.accept.split(',');
    if (type) {
      return headers.filter(h => {
        return h === type;
      });
    }
    return headers;
  } catch(e) {
    // When Chrome dev tool activate the headers become empty
    return [];
  }
};

/**
 * get document (string) byte length for use in header
 * @param {string} doc to calculate
 * @return {number} length
 */
const getDocLen = doc => {
  return Buffer.byteLength(doc, 'utf8');
};

/**
 * turn callback to promise
 * @param {string} p path to file
 * @return {object} promise to resolve
 */
const readDocument = p => {
  return new Promise((resolver, rejecter) => {
    fs.readFile(p, {encoding: 'utf8'}, (err, data) => {
      if (err) {
        return rejecter(err);
      }
      resolver(data);
    });
  });
};

/**
 * Search for the default index file
 * @param {object} config the serveStatic options
 * @return {string} path to the index file
 */
const searchIndexFile = config => {
  const { webroot, index } = config;
  const webroots = toArray(webroot);
  return webroots
    .map(d => [d, index].join('/'))
    .filter(fs.existsSync)
    .reduce((last, next) => {
      return next;
    }, null);
};

/**
 * Double check if its a HTML file
 * @param {string} file path
 * @return {boolean} or not
 */
const isHtmlFile = file => {
  const ext = path.extname(file).toLowerCase();
  return ext === '.html' || ext === '.htm';
};

/**
 * Make sure the supply argument is an array
 */
const toArray = param => {
  if (param) {
    return Array.isArray(param) ? param : [param];
  }
  return [];
};

// export
module.exports = {
  getHtmlDocument,
  injectToHtml,
  headerParser,
  getDocLen,
  readDocument,
  toArray,
  injectToHtml,
  getFilesToInject,
  tagCss,
  tagJs,
  processFiles,
  getSource
};
